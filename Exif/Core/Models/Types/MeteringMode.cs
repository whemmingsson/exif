﻿namespace ExifNET.Models.Types
{
    /// <summary>
    /// Models the metering mode.
    /// See <see href="http://en.wikipedia.org/wiki/Metering_mode">this article</see> for more information about these modes
    /// </summary>
    public enum MeteringMode
    {
        /// <summary>
        /// The metering mode is unknown
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// Measures the light from the whole scene
        /// </summary>
        Average,
        /// <summary>
        /// Measures the light on the center area and fethers the edges
        /// </summary>
        CenterWeightedAverage,
        /// <summary>
        /// Measures a very small portion of the scene
        /// </summary>
        Spot,
        /// <summary>
        /// Bases the measurement on several spots on the scene
        /// </summary>
        MultiSpot,
        /// <summary>
        /// Bases the measurement on several spots on the scene based on a pattern (?)
        /// </summary>
        Pattern,
        /// <summary>
        /// Measures the light in a larger area than "Spot" and sutiable for very dark or bright scenes
        /// </summary>
        Partial,
        /// <summary>
        /// Metering mode other than the predefined types
        /// </summary>
        Other = 255
    }
}
