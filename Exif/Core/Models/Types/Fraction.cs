﻿using System;
using System.Globalization;

namespace ExifNET.Models.Types
{
    /// <summary>
    /// Models a fraction for exact calculations
    /// </summary>
    public class Fraction
    {
        private int _enumerator;
        private int _denominator;

        /// <summary>
        /// Represents the enumerator in a fraction value
        /// </summary>
        public int Enumerator
        {
            get { return _enumerator; }
            set { _enumerator = value; }
        }

        /// <summary>
        /// Represents the denominator in a fraction value
        /// </summary>
        public int Denominator
        {
            get { return _denominator; }
            set
            {
                if (value.Equals(0))
                {
                    throw new DivideByZeroException("The denominator cannot be zero");
                }

                _denominator = value;
            }
        }

        /// <summary>
        /// Calculates the actual value using a double
        /// </summary>
        public double Value
        {
            get { return _enumerator/(double) _denominator; }
        }

        /// <summary>
        /// Overrides the ToString() to enable easy priting were ToString() is called automatically
        /// </summary>
        /// <returns>String representation of the fraction, using invariant culture</returns>
        public override string ToString()
        {
            return Value.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Creates the fraction using two integers representing the enumerator and denominator
        /// </summary>
        /// <param name="enumerator">Enumerator</param>
        /// <param name="denominator">Denominator</param>
        public Fraction(int enumerator, int denominator)
        {
            Enumerator = enumerator;
            Denominator = denominator;
        }
    }
}


