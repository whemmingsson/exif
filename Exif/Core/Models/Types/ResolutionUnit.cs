﻿namespace ExifNET.Models.Types
{
    /// <summary>
    /// The unit for measuring XResolution and YResolution.
    /// </summary>
    public enum ResolutionUnit
    {
        /// <summary>
        /// Inches
        /// </summary>
        Inches = 2,
        /// <summary>
        /// Centimeters
        /// </summary>
        Centimeters = 3
    }
}
