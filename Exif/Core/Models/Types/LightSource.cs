﻿namespace ExifNET.Models.Types
{
    /// <summary>
    /// Models a lightsource
    /// </summary>
    public enum LightSource
    {
        /// <summary>
        /// Lightsource is unknown
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// Daylight 
        /// </summary>
        Daylight,
        /// <summary>
        /// Flourecent light 
        /// </summary>
        Fluorescent,
        /// <summary>
        /// Tungsten light, eg from a light bulb
        /// </summary>
        Tungsten,
        /// <summary>
        /// Indicates the flash was used
        /// </summary>
        Flash,
        /// <summary>
        /// Outside, fine weather
        /// </summary>
        FineWeather,
        /// <summary>
        /// Outside, shade
        /// </summary>
        Shade,
        /// <summary>
        /// Daylight fluorescent (D 5700 – 7100K) 
        /// </summary>
        DaylightFluorescent,
        /// <summary>
        /// Day white fluorescent (N 4600 – 5400K)
        /// </summary>
        DayWhiteFluorescent,
        /// <summary>
        /// Cool white fluorescent (W 3900 – 4500K)
        /// </summary>
        CoolWhiteFluorescent,
        /// <summary>
        /// White fluorescent (WW 3200 – 3700K)
        /// </summary>
        WhiteFluorescent,
        /// <summary>
        /// Standard light A
        /// </summary>
        StandardLightA,
        /// <summary>
        /// Standard light B
        /// </summary>
        StandardLightB,
        /// <summary>
        /// Standard light C
        /// </summary>
        StandardLightC,
        /// <summary>
        /// D55
        /// See <see href="http://en.wikipedia.org/wiki/Standard_illuminant">this article</see> for more information about this light source setting
        /// </summary>
        D55,
        /// <summary>
        /// Illuminant D65
        /// See <see href="http://en.wikipedia.org/wiki/Illuminant_D65">this article</see> for more information about this light source setting
        /// </summary>
        D65,
        /// <summary>
        /// D75
        /// See <see href="http://en.wikipedia.org/wiki/Standard_illuminant">this article</see> for more information about this light source setting
        /// </summary>
        D75,
        /// <summary>
        /// D50
        /// See <see href="http://en.wikipedia.org/wiki/Standard_illuminant">this article</see> for more information about this light source setting
        /// </summary>
        D50,
        /// <summary>
        /// ?
        /// </summary>
        IsoStudioTungsten,
        /// <summary>
        /// Light source other than predefined types
        /// </summary>
        OtherLightSource = 255
    }
}
