﻿namespace ExifNET.Models.Types
{
    /// <summary>
    /// Models the exposure program type
    /// </summary>
    public enum ExposureProgram
    {
        /// <summary>
        /// Exposure program is undefined
        /// </summary>
        Undefined = 0,
        /// <summary>
        /// Manual exposure
        /// </summary>
        Manual,
        /// <summary>
        /// Normal exposure progran
        /// </summary>
        NormalProgram,
        /// <summary>
        /// Aperture priority (Av)
        /// </summary>
        AperturePriority,
        /// <summary>
        /// Shutter priority (Tv)
        /// </summary>
        ShutterPriority,
        /// <summary>
        /// Creative program
        /// </summary>
        CreativeProgram,
        /// <summary>
        /// Action program
        /// </summary>
        ActionProgram,
        /// <summary>
        /// Portrait mode
        /// </summary>
        PortraitMode,
        /// <summary>
        /// Landscape mode
        /// </summary>
        LandscapeMode,
    }
}
