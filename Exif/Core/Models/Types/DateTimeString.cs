﻿using System;

namespace ExifNET.Models.Types
{
    /// <summary>
    /// Special class that encapsulates some String to DateTime conversion functionality
    /// </summary>
    public class DateTimeString
    {
        private string _dateString;

        /// <summary>
        /// Creates the object based on a Exif string
        /// </summary>
        /// <param name="dateString">A date time string on the format "YY:MM:dd HH:mm:ss"</param>
        public DateTimeString(string dateString)
        {
            _dateString = ToParsableDate(dateString);
        }

        /// <summary>
        ///  Converts the Exif string format to a datetime parsable string
        /// </summary>
        /// <param name="stringData">Date as string, must be of format YY:MM:DD HH:MM:DD</param>
        /// <returns>Datetime string in the correct format for parsing</returns>
        private string ToParsableDate(string stringData)
        {
            return stringData.Substring(0, 11).Replace(":", "-") + stringData.Substring(12);
        }

        /// <summary>
        /// Returns the DateTime representation of this object
        /// </summary>
        public DateTime? Value
        {
            get
            {
                DateTime date;

                if (!string.IsNullOrWhiteSpace(_dateString) && DateTime.TryParse(_dateString, out date))
                {
                    return date;
                }

                return null;
            }
        }
    }
}
