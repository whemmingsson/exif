﻿namespace ExifNET.Models.Types
{
    /// <summary>
    /// Models an image rotation
    /// </summary>
    public enum Orientation
    {
        /// <summary>
        /// Standard (no) rotation
        /// </summary>
        Normal = 1,

        /// <summary>
        /// Rotated 90 degrees to the right (counter clockwise)
        /// </summary>
        Rotate90 = 6,

        /// <summary>
        /// Rotated 90 degrees to the left (clockwise)
        /// </summary>
        Rotate270 = 8,

        /// <summary>
        /// Rotated a full 180 degrees
        /// </summary>
        Rotate180 = 3
    }
}
