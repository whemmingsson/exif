﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using ExifNET.Extensions;
using ExifNET.Interfaces;
using ExifNET.Models.Parsers;
using ExifNET.Models.Types;
using System.IO;
using System.Drawing;

namespace ExifNET.Models.Base
{
    /// <summary>
    /// Exif Base containing core functionality
    /// </summary>
    public abstract class ExifBase
    {
        /// <summary>
        /// Contains the raw data from the PropertyItem array
        /// </summary>
        private readonly Dictionary<int, byte[]> _rawData;

        /// <summary>
        /// Contains the parsed data
        /// </summary>
        private readonly Dictionary<int, object> _parsedData; 

        /// <summary>
        /// Holds all parsers currently supported in the system
        /// </summary>
        private readonly List<IExifParser> _parsers;

        private ExifBase()
        {
            _rawData = new Dictionary<int, byte[]>();
            _parsedData = new Dictionary<int, object>();
            _parsers = new List<IExifParser>();

            CreateParsers();
        }

        /// <summary>
        /// Creates the Exif object based on a PropertyItems-array
        /// </summary>
        /// <param name="propertyItems">The list of all avaliable propertyItems for an image</param>
        protected ExifBase(IEnumerable<PropertyItem> propertyItems) : this()
        {
            LoadExifData(propertyItems);
        }

        /// <summary>
        /// Creates the exif object based on a file path.
        /// </summary>
        /// <param name="filePath">Path to the file</param>
        protected ExifBase(string filePath) : this()
        {       
            CreateExifDataFromPath(filePath);        
        }

        /// <summary>
        /// Creates the Exif object based on a file path
        /// </summary>
        /// <param name="path"></param>
        private void CreateExifDataFromPath(string path)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException("The file provided in the constructor does not exist. Verify that is an absolute path and that the file is not used by another process.");
            }
            using (Stream stream = File.Open(path, FileMode.Open))
            {
                LoadExifData(new Bitmap(stream).PropertyItems);
            }
        }

        /// <summary>
        /// Setup the standard parsers
        /// </summary>
        private void CreateParsers()
        {
            AddDefaultParsers();
            AddCustomParsers();
            AddEnumParsers();
        }
 
        private void AddDefaultParsers()
        {
            _parsers.Add(new AsciiParser());
            _parsers.Add(new RationalParser());
            _parsers.Add(new ShortParser());
        }

        private void AddCustomParsers()
        {
            _parsers.Add(new DateTimeParser());
            _parsers.Add(new FractionParser());
        }

        private void AddEnumParsers()
        {
            _parsers.Add(new EnumParser<LightSource>());
            _parsers.Add(new EnumParser<ExposureProgram>());
            _parsers.Add(new EnumParser<MeteringMode>());
            _parsers.Add(new EnumParser<ResolutionUnit>());
            _parsers.Add(new EnumParser<Orientation>());
        }

        /// <summary>
        /// Prepends the provided parser to avaliable parsers
        /// </summary>
        /// <param name="parser">A custom parser implementing the IExifParser interface</param>
        protected void AddCustomParser(IExifParser parser)
        {
            _parsers.Insert(0, parser);
        }

        /// <summary>
        /// Store the ProperyItem EXIF data in an internal array.
        /// </summary>
        /// <param name="propertyItems">The list of all avaliable propertyItems for an image</param>
        private void LoadExifData(IEnumerable<PropertyItem> propertyItems)
        {
            foreach (var propertyItem in propertyItems)
            {
                _rawData.Add(propertyItem.Id, propertyItem.Value);
            }
        }

        /// <summary>
        /// Performs a datatype lookup, selects an approperiate parser and parses the raw byte[] data
        /// </summary>
        /// <typeparam name="ExifValueType">Type to parse to</typeparam>
        /// <param name="tag">Tag ID for the property</param>
        /// <returns>Parsed value or null</returns>
        private ExifValueType GetParsedValue<ExifValueType>(int tag)
        {
            var parser = GetParserForType<ExifValueType>();

            byte[] rawData;
            if (_rawData.TryGetValue(tag, out rawData) && parser != null)
            {
                try { return (ExifValueType)parser.Parse(rawData); }
                catch (Exception) { return default(ExifValueType); }
            }

            return default(ExifValueType);
        }

        private IExifParser GetParserForType<ParserType>()
        {
            return _parsers.DefaultIfEmpty(new AsciiParser()).First(p => p.Type == typeof(ParserType));
        }

        /// <summary>
        /// Look up the value in the local dictionary, if not present it adds the parsed value to it for later use
        /// </summary>
        /// <typeparam name="ParserType">Type to parse to</typeparam>
        /// <param name="tag">Tag ID for the property</param>
        /// <returns>Parsed value or null</returns>
        protected ParserType GetValue<ParserType>(int tag)
        {
            object result;
            if (_parsedData.TryGetValue(tag, out result))
            {
                return (ParserType)result;
            }

            var parsedValue = GetParsedValue<ParserType>(tag);
            _parsedData.Add(tag, parsedValue);
            return parsedValue;
        }

        /// <summary>
        /// Returns a list of all avaliable parser types for the Exif object
        /// </summary>
        /// <returns>A list of types for active parsers</returns>
        public IEnumerable<Type> GetParserTypes()
        {
            var result = new List<Type>();

            foreach (var exifParser in _parsers)
            {
                result.AddUnique(exifParser.Type);
            }

            return result;
        }

        /// <summary>
        /// Returns the raw byte[] data for the provided tag id.
        /// </summary>
        /// <param name="tag">Tag id representing the data value</param>
        /// <returns>Byte[] containing raw data or null if not avaliable</returns>
        public byte[] GetRawData(int tag)
        {
           byte[] result;
           return _rawData.TryGetValue(tag, out result) ? result : null;
        }
    }
}
