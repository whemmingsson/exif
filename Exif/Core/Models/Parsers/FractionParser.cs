﻿using System;
using ExifNET.Interfaces;
using ExifNET.Models.Types;

namespace ExifNET.Models.Parsers
{
    /// <summary>
    /// Parses raw data in to a special Fraction object
    /// </summary>
    class FractionParser : IExifParser
    {
        public Type Type
        {
            get { return typeof (Fraction); }
        }

        public object Parse(byte[] exifData)
        {
            return new Fraction(exifData[0], exifData[4]);
        }
    }
}
