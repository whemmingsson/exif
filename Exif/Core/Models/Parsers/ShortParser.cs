﻿using System;
using ExifNET.Interfaces;

namespace ExifNET.Models.Parsers
{
    /// <summary>
    /// Parser that deals with converting byte[] to short int
    /// </summary>
    class ShortParser : IExifParser
    {
        public Type Type { get { return typeof(short?); } }

        public object Parse(byte[] exifData)
        {
            return BitConverter.ToInt16(exifData, 0);
        }
    }
}
