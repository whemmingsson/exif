﻿using System;
using ExifNET.Extensions;
using ExifNET.Interfaces;

namespace ExifNET.Models.Parsers
{
    /// <summary>
    /// Parser that deals with converting byte[] to float
    /// </summary>
    class RationalParser : IExifParser
    {
        public Type Type { get { return typeof(float?); } }

        public object Parse(byte[] exifData)
        {
            var e = BitConverter.ToInt32(exifData.SubArray(0, 4), 0);
            var d = BitConverter.ToInt32(exifData.SubArray(4, 4), 0);
            return e/(float)d;
        }
    }
}
