﻿using System;
using System.Linq;
using ExifNET.Interfaces;

namespace ExifNET.Models.Parsers
{
    /// <summary>
    /// Parser that deals with converting byte[] to string
    /// </summary>
    class AsciiParser : IExifParser
    {
        public Type Type
        {
            get { return typeof(string); }
        }

        public object Parse(byte[] exifData)
        {
            return exifData.Aggregate(string.Empty, (current, b) => current + (char)b).Replace("\0", string.Empty);
        }
    }
}
