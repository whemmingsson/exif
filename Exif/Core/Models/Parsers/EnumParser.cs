﻿using System;
using ExifNET.Interfaces;

namespace ExifNET.Models.Parsers
{
    /// <summary>
    /// Creates a parser to deal with enum types
    /// </summary>
    /// <typeparam name="T">T must be an enumerated type</typeparam>
    class EnumParser<T> : IExifParser where T : struct, IConvertible
    {
        public Type Type { get; private set; }

        public object Parse(byte[] exifData)
        {
            var value = BitConverter.ToInt16(exifData, 0);
            return Convert.ToInt32(value);
        }

        /// <summary>
        /// Constructs the parser object
        /// <exception cref="System.ArgumentException">Thrown if the provided type T is not enumerable</exception>
        /// </summary>
        public EnumParser()
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("T must be an enumerated type");
            }

            Type = typeof (T);
        }
    }
}
