﻿using System;
using System.Linq;
using ExifNET.Interfaces;
using ExifNET.Models.Types;

namespace ExifNET.Models.Parsers
{
    /// <summary>
    /// Parser that deals with converting byte[] to Date Time
    /// </summary>
    class DateTimeParser : IExifParser
    {
        public Type Type { get { return typeof(DateTime?); } }

        public object Parse(byte[] exifData)
        {
            var dateTimeString = new DateTimeString(exifData.Aggregate(string.Empty, (current, b) => current + (char)b).Replace("\0", string.Empty));
            return dateTimeString.Value;
        }
    }
}
