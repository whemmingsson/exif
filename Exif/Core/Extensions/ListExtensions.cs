﻿using System.Collections.Generic;
using System.Linq;

namespace ExifNET.Extensions
{
    /// <summary>
    /// Extension class providing helper function for list management
    /// </summary>
    static class ListExtensions
    {
        /// <summary>
        /// Adds an item to the list if the item is not already present in the list
        /// </summary>
        /// <typeparam name="T">Type of items in list</typeparam>
        /// <param name="list">The ICollection list to add item to</param>
        /// <param name="item">The item to add</param>
        internal static void AddUnique<T>(this ICollection<T> list, T item)
        {
            if (!list.Any(i => i.Equals(item)))
            {
                list.Add(item);
            }
        }
    }
}
