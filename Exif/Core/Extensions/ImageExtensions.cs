﻿using System.Drawing;

namespace ExifNET.Extensions
{
    /// <summary>
    /// Static class that provides an extension method for the Image class to create the Exif object
    /// </summary>
    public static class ImageExtensions
    {
        /// <summary>
        /// Get the Exif object from an image
        /// </summary>
        /// <param name="image">Image object</param>
        /// <returns>The new Exif object</returns>
        public static Exif GetExif(this Image image)
        {
            return new Exif(image.PropertyItems);
        }
    }
}
