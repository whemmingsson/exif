﻿using System;

namespace ExifNET.Extensions
{
    /// <summary>
    /// Contains extension methods for arrays
    /// </summary>
    public static class ArrayExtensions
    {
        /// <summary>
        /// Creates a sub array
        /// </summary>
        /// <typeparam name="T">Type of data in array</typeparam>
        /// <param name="data">Data itself</param>
        /// <param name="index">Starting index</param>
        /// <param name="length">Length of new array</param>
        /// <returns>A sub array</returns>
        public static T[] SubArray<T>(this T[] data, int index, int length)
        {
            var result = new T[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }
    }
}
