﻿using System;

namespace ExifNET.Interfaces
{
    /// <summary>
    /// Interface for parsers to implement
    /// </summary>
    public interface IExifParser
    {
        /// <summary>
        /// Type that the parser should deal with
        /// </summary>
        Type Type { get; }

        /// <summary>
        /// Parser method that converts the byte[] to object of type Type
        /// </summary>
        /// <param name="exifData">The Exif data, as an byte[], to parse</param>
        /// <returns>Parsed object of type Type</returns>
        object Parse(byte[] exifData);
    }
}
