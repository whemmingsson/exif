﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using ExifNET.Models.Base;
using ExifNET.Models.Types;

namespace ExifNET
{
    /// <summary>
    /// Exif data structure that holds all public properties
    /// </summary>
    public class Exif : ExifBase
    {
        #region *************************** Constructor ***************************
        /// <summary>
        /// Creates the Exif object based on a PropertyItems-array
        /// </summary>
        /// <param name="propertyItems">The list of all avaliable propertyItems for an image</param>
        public Exif(IEnumerable<PropertyItem> propertyItems) : base(propertyItems){}

        /// <summary>
        /// Creates the exif object based on a path to an image file. Relative to the running application or absolute.
        /// </summary>
        /// <param name="filePath">Path to file resource</param>
        public Exif(string filePath) : base(filePath) {}

        #endregion

        #region ************************* Short properties ************************

        /// <summary>
        /// The ISO value.
        /// </summary>
        public short? IsoSpeedRatings
        {
            get{ return GetValue<short?>(34855); }
        }

        #endregion

        #region *********************** Float/fraction properties **********************

        /// <summary>
        /// Exposure time, given in seconds (sec). 
        /// </summary>
        public Fraction ExposureTime
        {
            get { return GetValue<Fraction>(33434); }
        }

        /// <summary>
        /// The F number
        /// </summary>
        public Fraction FNumber
        {
            get { return GetValue<Fraction>(33437); }
        }

        /// <summary>
        /// The lens aperture. The unit is the APEX value
        /// </summary>
        public float? ApertureValue
        {
            get { return GetValue<float?>(37378); }
        }

        /// <summary>
        /// The smallest F number of the lens. The unit is the APEX value.
        /// </summary>
        public float? MaxApertureValue
        {
            get { return GetValue<float?>(37381); }
        }

        /// <summary>
        /// The actual focal length of the lens, in mm. Conversion is not made to the focal length of a 35 mm film camera. 
        /// </summary>
        public float? FocalLength
        {
            get { return GetValue<float?>(37386); }
        }

        /// <summary> 
        ///Indicates the strobe energy at the time the image is captured, as measured in Beam Candle Power Seconds (BCPS). 
        /// </summary>
        public float? FlashEnergy
        {
            get { return GetValue<float?>(41483); }
        }

        /// <summary>  
        ///Indicates the exposure index selected on the camera or input device at the time the image is captured. 
        /// </summary>
        public float? ExposureIndex
        {
            get { return GetValue<float?>(41493); }
        }

        /// <summary>  
        /// The distance to the subject, given in meters
        /// </summary>
        public float? SubjectDistance
        {
            get { return GetValue<float?>(37382); }
        }

        /// <summary>  
        /// The number of pixels per ResolutionUnit in the ImageWidth direction
        /// </summary>
        public float? XResolution
        {
            get { return GetValue<float?>(282); }
        }

        /// <summary>  
        /// The number of pixels per ResolutionUnit in the ImageLength direction ResolutionUnit  
        /// </summary>
        public float? YResolution
        {
            get { return GetValue<float?>(283); } 
        }

         /// <summary>  
        /// Shutter speed. The unit is the APEX (Additive Systemof Photographic Exposure) setting.
        /// </summary>
        public float? ShutterSpeedValue
        {
            get { return GetValue<float?>(37377); }
        }

        #endregion

        #region ************************* String properties ***********************

        /// <summary>
        /// A character string giving the title of the image
        /// </summary>
        public string ImageDescription
        {
            get { return GetValue<string>(270); }
        }

        /// <summary>
        /// The model name or model number of the equipment.
        /// </summary>
        public string Model
        {
            get { return GetValue<string>(272); }
        }

        /// <summary>
        /// The manufacturer of the recording equipment. 
        /// </summary>
        public string Make
        {
            get { return GetValue<string>(271); }
        }

        /// <summary>
        /// Records the name and version of the software or firmware of the camera or image input device used to generate the image
        /// </summary>
        public string Software
        {
            get { return GetValue<string>(305); }
        }

        /// <summary>
        /// Name of the camera owner, photographer or image creator
        /// </summary>
        public string Artist
        {
            get { return GetValue<string>(315); }
        }

        /// <summary>
        /// Copyright information
        /// </summary>
        public string Copyright
        {
            get { return GetValue<string>(33432); }
        }

        /// <summary>  
        /// The current Exif version
        /// </summary>
        public string ExifVersion
        {
            get { return GetValue<string>(36864); }
        }

        /// <summary>  
        /// Indicates the spectral sensitivity of each channel of the camera used
        /// </summary>
        public string SpectralSensitivity
        {
            get { return GetValue<string>(34852); }
        }

        /// <summary>
        /// Indicates the Opto-Electric Conversion Function (OECF) specified in ISO 14524. OECFis the relationship between the camera optical input and the image values.
        /// </summary>
        public string OECF
        {
            get { return GetValue<string>(34856); }
        }

        #endregion

        #region ************************* Special properties **********************

        /// <summary>
        /// Date time value for the image
        /// </summary>
        public DateTime? DateTime
        {
            get { return GetValue<DateTime?>(306); }
        }

        /// <summary>
        /// The date and time when the original image data was generated.
        /// </summary>
        public DateTime? DateTimeOriginal
        {
            get { return GetValue<DateTime?>(36867); }
        }

        /// <summary>
        /// The date and time when the image was stored as digital data.
        /// </summary>
        public DateTime? DateTimeDigitized
        {
            get { return GetValue<DateTime?>(36868); }
        }

        /// <summary>
        /// The metering mode.
        /// </summary>
        public MeteringMode MeteringMode
        {
            get { return GetValue<MeteringMode>(37383); }
        }

        /// <summary>
        /// The light source.
        /// </summary>
        public LightSource LightSource
        {
            get { return GetValue<LightSource>(37384); }
        }

        /// <summary>
        /// The exposure program
        /// </summary>
        public ExposureProgram ExposureProgram
        {
            get { return GetValue<ExposureProgram>(34850); }
        }

        /// <summary>  
        /// The unit for measuring XResolution and YResolution.
        /// </summary>
        public ResolutionUnit ResolutionUnit
        {
            get { return GetValue<ResolutionUnit>(296); }
        }

        /// <summary>  
        /// The exposure bias. The unit is the APEX value. Ordinarily it is given in the range of –99.99 to 99.99
        /// </summary>
        public float? ExposureBias
        {
            get { return GetValue<float?>(37380); }
        }

        /// <summary>
        /// The image rotation/orientation
        /// </summary>
        public Orientation Orientation
        {
            get { return GetValue<Orientation>(274); }
        }

        #endregion
    }
}
