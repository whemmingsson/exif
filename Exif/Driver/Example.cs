﻿using System;
using System.Drawing;
using System.IO;
using ExifNET;

namespace Example
{
    class SmallExample
    {
        public void Run()
        {
            // Init the exif object
            var stream = File.Open("test.jpg", FileMode.Open);
            var img = new Bitmap(stream);
            var exif = new Exif(img.PropertyItems);

            // Print some properties
            Console.WriteLine("Made by: " + exif.Artist);
            Console.WriteLine("ISO: " + exif.IsoSpeedRatings);         
        }
    }
}
