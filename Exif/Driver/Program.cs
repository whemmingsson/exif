﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using ExifNET;
using ExifNET.Models.Types;

namespace Driver
{
    class Program
    {
        static void Main()
        {
            // Init
            //var s = File.Open("test.jpg", FileMode.Open);
            //var img = new Bitmap(s);
            //var exif = new Exif(img.PropertyItems);

            var exif = new Exif("test.jpg");

            switch(exif.Orientation)
            {
            case Orientation.Normal: { Console.WriteLine("No rotation"); }break;
            case Orientation.Rotate90: { Console.WriteLine("Rotated 90 degrees CW"); } break;
            case Orientation.Rotate180: { Console.WriteLine("Rotated 180 degrees"); } break;
            case Orientation.Rotate270: { Console.WriteLine("Rotated 270 degress CW"); } break;
            }

            // Show property values
            Console.WriteLine("-- PROPERTIES --");
            var type = exif.GetType();
            var properties = type.GetProperties().OrderBy(p => p.Name).ToArray();
            foreach (var property in properties)
            {
                var val = property.GetValue(exif, null);
                if (val == null)
                {
                    Console.WriteLine(property.Name + ": N/A");
                }
                else
                {
                    Console.WriteLine(property.Name + ": " + val);
                }
            }

            // Show all avaliable types to parse
            Console.WriteLine("\n-- PARSER TYPES --");
            foreach (var parserType in exif.GetParserTypes())
            {
                Console.WriteLine(parserType.ToString());
            }

            Console.ReadKey();
        }
    }
}
