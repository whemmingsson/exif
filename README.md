# Exif Core #


## 1. Overview ##

The goal of this project is to make a library that can take the metadata from the .NET Image class stored in the PropertyItems array and turn it into a user friendly object.

The issue arises when there is no inherit way to parse the data. There is also no way of mapping the data to some kind of descriptive label. 
This is accomplished trough a compact Exif class where its public properties maps to Exif properties.

## 2. The Exif specification ##

The Exif specification can be found here: http://www.exiv2.org/Exif2-2.PDF [referred to as SPEC in this document]

From the SPEC we can conclude that the following possible data types exists:

1. BYTE An 8-bit unsigned integer.,
2. ASCII An 8-bit byte containing one 7-bit ASCII code. The final byte is terminated with NULL.,
3. SHORT A 16-bit (2-byte) unsigned integer,
4. LONG A 32-bit (4-byte) unsigned integer,
5. RATIONAL Two LONGs. The first LONG is the numerator and the second LONG expresses the denominator.,
7. UNDEFINED An 8-bit byte that can take any value depending on the field definition,
8. SLONG A 32-bit (4-byte) signed integer (2's complement notation),
9. SRATIONAL Two SLONGs. The first SLONG is the numerator and the second SLONG is the denominator

(Source: Exif2-2.pdf, page 14).

We can also see that there are a wide range of different meta data entries that could be located
within the Exif data. Examples are meta data such as ISO, F-stop and Exposure time. All these have
unique ID's that will be referred to as TAG in this document.

## 3. Implementation ##

### 3.1 General ###

Each entry in the PropertyItems array maps to a specific property in an instance of 
the Exif object. 

Example 1:


```
#!c#

// Declare the object and create the instance.
var exif = new Exif(image.PropertyItems);

// Print the ISO value (of type <short?>)
console.Write("ISO: " + exif.IsoSpeedRatings);
```

Example 2:


```
#!c#

// Declare the object and create the instance.
var exif = new Exif("myFile.jpg");

// Print the current Exif version
console.Write("Current exif version: " + exif.ExifVersion);
```

This gives good flexibility for consumers. The Exif SPEC is hidden within the functionality
of the Exif object. 

Basically it works as follows. When the Exif object is created:

1. Iterate over all PropertyItems provided to the constructor
2. Store the raw data in an internal data structure

When the consumer calls for an property:

1. Lookup the tag in an internal dictionary of parsed values, and returns the value if present. If not,
2. Use the provided .NET type to retrieve a Exif Parser object
3. Lookup the raw data based on the appropriate TAG value
4. Parse the raw data of type TYPE into the actual value of the .NET type.
5. Store the value in the dictionary and return it.

Following is the implementation of an Exif property
```
#!c#
/// <summary>
/// The ISO value.
/// </summary>
public short? IsoSpeedRatings
{
      get{return GetValue<short?>(34855);}
}
```

The TAG value of 34855 can be found in the SPEC. To add more properties, all that is needed is to give it a good name, a nullable return type, and call the GetValue() method with this type and the corresponding TAG.

### 3.2 Parsing

Every Exif Parser class must implement a simple interface:

```
#!c#

interface IExifParser
{
      Type Type { get; }
      object Parse(byte[] exifData);
}
```

In other words, a valid Exif Parser must know about what type to parse to, as well as implement the Parse method it self. The Type property is used for lookup - so that the Exif class can retrieve the proper parser. All available parsers are added in the Exif method CreateParsers():


```
#!c#

private void CreateParsers()
{
    // Add default parsers
    _parsers.Add(new AsciiParser());
    _parsers.Add(new RationalParser());
    _parsers.Add(new ShortParser());

    // Add custom parsers
    _parsers.Add(new DateTimeParser());
    _parsers.Add(new FractionParser());

    // Add custom enum parsers
    _parsers.Add(new EnumParser<LightSource>());
    _parsers.Add(new EnumParser<ExposureProgram>());
    _parsers.Add(new EnumParser<MeteringMode>());
    _parsers.Add(new EnumParser<ResolutionUnit>());
    _parsers.Add(new EnumParser<Orientation>());
}
```

## 4. Dependencies ##
The Exif core library have by design very few dependencies to other assemblies and libraries. In the current version, the following namespaces in .NET are used:

* System
* System.Core
* System.Drawing *
* System.Linq
* System.Collections.Generic

** The System.Drawing assembly is only used to gain access to the PropertyItems type.*


## 5. Extensibility ##

The Exif implementation can easily be extended. It's possible to inherit the Exif class and add more properties, by following the convention described in this document. It's also possible for classes inheriting the Exif class to call the method AddCustomParser(), for example in the constructor. 


```
#!c#

/// <summary>
/// Prepends the provided parser to available parsers
/// </summary>
/// <param name="parser">A custom parser implementing the IExifParser interface</param>
protected void AddCustomParser(IExifParser parser)
{
	_parsers.Insert(0, parser);
}
```

This way it's possible to override the standard implementation of any built-in parser. This is due to the fact that the usage of the _parsers list is as follows:

```
#!c#

// Find the first parser in the list that parses the exif raw data to an object of type T
var parser = _parsers.FirstOrDefault(p => p.Type == typeof(T));
```

So if a new parser is added that converts the raw data to, for example, a string, it will be used because it appears earlier in the list of all parsers.